package app;


import java.awt.Event;
import java.io.File;
import java.io.InputStream;

import com.googlecode.lanterna.TerminalSize;
import com.googlecode.lanterna.terminal.Terminal;

import javafx.application.Application;
import model.EventHandler;
import model.SceneManager;
import util.Config;
import util.Musicplayer;
import util.MyDefaultFactory;

public class Game {
	public static Config config;
	public EventHandler eventhandler;
	public MyDefaultFactory factory= new MyDefaultFactory();;
	public static Terminal term;
	public SceneManager sceneManager;
	public Musicplayer player;
	public void Launch() {
		init();
	}
	public void init() {
		try {
			config=new Config(this);
			term = factory.createTerminal();
			eventhandler=new EventHandler(this);
			sceneManager= new SceneManager(this);
			Musicplayer player= new Musicplayer();
			Application.launch(player.getClass());
		
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
	}

	

	


	 

}
