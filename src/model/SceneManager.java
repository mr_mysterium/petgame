package model;

import java.awt.Menu;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Hashtable;

import com.googlecode.lanterna.TerminalPosition;
import com.googlecode.lanterna.TerminalSize;
import com.googlecode.lanterna.gui2.AbsoluteLayout;
import com.googlecode.lanterna.gui2.BasicWindow;
import com.googlecode.lanterna.gui2.BorderLayout;
import com.googlecode.lanterna.gui2.Label;
import com.googlecode.lanterna.gui2.MultiWindowTextGUI;
import com.googlecode.lanterna.gui2.Panel;
import com.googlecode.lanterna.gui2.Window;
import com.googlecode.lanterna.gui2.WindowBasedTextGUI;
import com.googlecode.lanterna.screen.Screen;
import com.googlecode.lanterna.screen.TerminalScreen;
import com.googlecode.lanterna.screen.Screen.RefreshType;

import app.Game;
import util.Config;
import model.ui.scenes.*;
import model.ui.scenes.scenemodel.Scene;

public class SceneManager {
	public Game game;
	public  static Screen screen;
	public static  MultiWindowTextGUI gui;
	public static MenuScene menu;
	public static MainScene main;
	public static StatScene stats;
	public static MakeScene make;
	public static ArrayList<Scene>scenes;
	public static int currentscene=0;
	public static long lastframe=System.currentTimeMillis();
	public SceneManager(Game game) {
		try {
			this.game=game;
			screen = new TerminalScreen(game.term);
			gui = new MultiWindowTextGUI(screen);
			menu=new MenuScene();
			main= new MainScene();
			stats= new StatScene();
			make= new MakeScene();
			screen.startScreen();
			scenes = new ArrayList<Scene>(Arrays.asList(menu,main,stats,make));
			for (Scene scene : scenes)gui.addWindow(scene.window);
			menu.activate();
			gui.updateScreen();
			screen.refresh();
			EventHandler.input.start();
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	
	}
	
	public static void updateScenes() {
		for (Scene scene : scenes) {
			scene.update((System.currentTimeMillis()-lastframe)/1000.0);
		}
		lastframe=System.currentTimeMillis();
	}
	
	public static void changeScene(int index)  {
		EventHandler.clickables.clear();
		try {
			scenes.get(index).activate();
			currentscene=index;
		    gui.updateScreen();
			screen.refresh(RefreshType.COMPLETE);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}
