package model.ui.scenes;

import java.awt.MouseInfo;
import java.text.DecimalFormat;
import java.text.NumberFormat;

import com.googlecode.lanterna.Symbols;
import com.googlecode.lanterna.TerminalPosition;
import com.googlecode.lanterna.TextColor;
import com.googlecode.lanterna.graphics.SimpleTheme;

import model.Clickable;
import model.EventHandler;
import model.SceneManager;
import model.ui.components.TopBar;
import model.ui.scenes.scenemodel.DefaultScene;
import pet.Pet;
import model.ui.components.Button;
import model.ui.components.Container;
import model.ui.components.ProgressBar;
import util.Sprite;
import util.Symbol;
import util.TextUtil;

public class MainScene extends DefaultScene {
	Button pet;
	Container growlabel;
	Container patlabel;
	Container notification;
	ProgressBar xpbar;
	Container level;
	Button upgradebtn;
	Container needs;
	double growth=0;
	double pats=0;
	public MainScene() {
	super(1,"   MAIN   ");
	
	pet=new Button(Sprite.BIGDUCK,new TerminalPosition(28, 8),false);
	pet.sethoverable(false);
	upgradebtn=new Button("UPGRADE"+Symbol.UPARROW,new TerminalPosition(28, 12),false);
	upgradebtn.getcomponent().setVisible(false);
	upgradebtn.addClickListener(new Clickable() {
		
		@Override
		public void onClick() {
			upgradebtn.getcomponent().setVisible(false);
			upgradebtn.unlight();
			Pet.level++;
			Pet.xp=0;
			EventHandler.removeClickable(upgradebtn);
		}
	});
	growlabel=new Container("GROWING:\n", new TerminalPosition(28,12),false);
	patlabel=new Container("PATTING:\n",new TerminalPosition(28, 12),false);
	notification=new Container("",new TerminalPosition(0,18	), false);
	xpbar= new ProgressBar(10,0f, new TerminalPosition(54, 18),false);
	level = new Container("LVL       ",new TerminalPosition(54, 17), false);
	needs= new Container(" 0"+Symbol.SMILEY, new TerminalPosition(61,2),false);
	
	add(pet,growlabel,patlabel,upgradebtn,notification,level,xpbar,needs);
	}
	
	@Override
	public void activate() {
		super.activate();
		EventHandler.addClickable(pet);
		if (Pet.xp>=Pet.levels[Pet.level]) {
			EventHandler.addClickable(upgradebtn);
		}
	}
	@Override
	public void update(double deltatime) {
		super.update(deltatime);
		xpbar.setprogress((float)Pet.xp/Pet.levels[Pet.level]);
		
		
		String lvl = new String(Pet.level+"");
		String happiness=new String(Pet.happiness+"");
		level.combine(level.getText().length()-lvl.length(), 0, lvl);
		needs.combine(2-happiness.length(), 0, happiness);
		
		
		if (EventHandler.intersecting().contains(pet)&&EventHandler.mousedown&&EventHandler.mousebutton==1&&Pet.xp<Pet.levels[Pet.level])
			{
			grow(50*deltatime);
			}
		else {
			growth=0;
			growlabel.setText("");
		}
		
		if (!upgradebtn.getcomponent().isVisible()&&Pet.xp>=Pet.levels[Pet.level]) {
			upgradebtn.getcomponent().setVisible(true);
			EventHandler.addClickable(upgradebtn);
		}
		
		
		if (EventHandler.intersecting().contains(pet)&&EventHandler.mousedown&&EventHandler.mousebutton==3) {
			pat(EventHandler.mousedelta*deltatime*2);
		}else {
			pats=0;
			patlabel.setText("");
		}
		
		
		
		
		
		fade(deltatime);
	}
	
	public void grow(double amount) {
		if (growth<100) {
			  growth+=amount;
			  String formatted = String.format("%02d",(int)Math.min(99, growth));
			  growlabel.setText("GROWING:\n  "+formatted+" %");
		}else {
			if (Pet.xp+Pet.happiness<Pet.levels[Pet.level]-1) {
				Pet.xp=Pet.xp+Pet.happiness+1;
				if (Pet.happiness>0) {
					if (Math.random()<0.5) {
						notify("MEPHISTO GAINED "+(1+Pet.happiness)+" XP AND LOST 1"+Symbol.SMILEY);
						Pet.happiness--;
					}else {
						notify("MEPHISTO GAINED "+(1+Pet.happiness)+" XP");
					}
				}else {
					notify("MEPHISTO GAINED 1 XP");
				}
				
			}else {
				Pet.xp=Pet.levels[Pet.level];
				notify("MEPHISTO CAN LEVEL UP!");
			}
			growth=0;
			
		} 
	
	}
	
	public void pat(double amount) {
		if (pats<100) {
			pats+=amount;
			String formatted = String.format("%02d",(int)Math.min(99, pats));
			patlabel.setText("PATTING:\n  "+formatted+" %");	
		}else {
			pats=0;
			Pet.happiness++;
			notify("MEPHISTO GAINED 1"+Symbol.SMILEY);
		}
	}
	
	public void notify(String text) {
		notification.setText(text);
		notification.setbrightness(1);
	}
	
	private void fade(double deltatime) {
		
		if (notification.getbrightness()<=0.01) {
			notification.setbrightness(0);
			notification.setText("");
		}
		
		if (notification.getText().length()>=1) {
			notification.setbrightness(Math.pow(notification.getbrightness()-0.0005*deltatime, 1.1));
		}else {
			notification.setbrightness(1);
		}
	
	}
	
}
