package model.ui.scenes;

import java.awt.Point;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.googlecode.lanterna.TerminalPosition;
import com.googlecode.lanterna.TerminalSize;
import com.googlecode.lanterna.gui2.AbsoluteLayout;
import com.googlecode.lanterna.gui2.BorderLayout;
import com.googlecode.lanterna.gui2.Label;
import com.googlecode.lanterna.gui2.LayoutManager;

import model.EventHandler;
import model.SceneManager;
import model.ui.components.TopBar;
import model.ui.scenes.scenemodel.DefaultScene;
import model.ui.components.Button;
import model.ui.components.Container;
import util.Sprite;
import util.Symbol;
import util.TextUtil;

public class StatScene extends DefaultScene{
	
	
	
	Container infoBox = new Container("", new TerminalPosition(0,2), false);
	
	Container statBox = new Container("", new TerminalPosition(38,2),false);
	long start;
	
	public StatScene() {
		super(2,"   INFO   ");	
		infoBox.setText(TextUtil.generateBorder(new Point(26, 8),new String[] {"0,0,0,7","25,0,25,7","0,0,25,0","0,2,25,2","0,7,25,7","9,2,9,7"}));
		infoBox.combine(1, 1, "INFO:");
		infoBox.combine(1, 3, Sprite.BIGDUCK);
		infoBox.combine(10, 3, "NAME   MEPHISTO");
		infoBox.combine(10, 4, "TYPE   DUCK");
		infoBox.combine(10, 5, "RARITY COMMON");
		infoBox.combine(10, 6, "AGE    0s");
		
		statBox.setText(TextUtil.generateBorder(new Point(26, 8),new String[] {"0,0,0,7","25,0,25,7","0,0,25,0","0,2,25,2","0,7,25,7"}));	
		statBox.combine(1, 1, "STATS: ");
		statBox.combine(1, 3, "LVL   0");
		statBox.combine(1, 4, "XP 0/10");
		
		
		
		
		add(infoBox,statBox);
		start=System.currentTimeMillis();
	}
	
	
	@Override
	public void update(double deltatime) {
		super.update(deltatime);
		long time= System.currentTimeMillis();
		long age=time-start;
		String agetext=TextUtil.timeString(age);
		infoBox.combine(10, 6, TextUtil.repeat(" ", 15));
		infoBox.combine(10,6,"AGE    "+agetext+" ");
	}

}
