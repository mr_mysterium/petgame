package model.ui.scenes;

import java.awt.Point;

import com.googlecode.lanterna.TerminalPosition;

import model.Clickable;
import model.EventHandler;
import model.SceneManager;
import model.ui.components.Button;
import model.ui.components.Container;
import model.ui.components.ProgressBar;
import model.ui.components.TopBar;
import model.ui.scenes.scenemodel.DefaultScene;
import pet.Pet;
import util.Sprite;
import util.Symbol;
import util.TextUtil;

public class MakeScene extends DefaultScene {
	public Container itemlist;
	public Container hline;
	public Container ingredients;
	public Container img;
	public Container description;
	public ProgressBar pb;
	public Button makebtn;
	
	public Container status;
	public Container timer;
	public long beginning=System.currentTimeMillis();
	public boolean crafting=false;
	public double timerequirement=10;
	public double progress=0;
	public MakeScene() {
		super(3,"   MAKE   ");
		
		itemlist=new Container(TextUtil.generateBorder(new Point(11,16),new String[] {"10,2,10,15","0,1,10,1","0,3,10,3","0,5,10,5"}),new TerminalPosition(0,2),false);
		itemlist.combine(0, 0, "COIN");
		itemlist.combine(0, 2, "ITEM1");
		itemlist.combine(0, 4, "ITEM2");
		
		hline=new Container(TextUtil.repeat(Symbol.HORIZONTAL, 64), new TerminalPosition(0, 17),false);
		hline.combine(10, 0, Symbol.UPHORIZONTAL);
		
		ingredients=new Container(TextUtil.generateBorder(new Point(14,14),new String[] {"0,0,13,0","13,0,13,13","0,13,13,13","0,0,0,13"}), new TerminalPosition(50, 2), false);
		ingredients.combine(1, 1, "INGREDIENTS:");
		ingredients.combine(1, 2, "NONE");
		
		img=new Container(TextUtil.generateBorder(new Point(6,4),new String[] {"0,0,5,0","5,0,5,3","0,0,0,3"}), new TerminalPosition(12, 2), false);
		img.combine(0, 3, Symbol.UPRIGHTYOFFSET+TextUtil.repeat(Symbol.HORIZONTALYOFFSET, 4)+Symbol.UPLEFTYOFFSET);
		img.combine(0, 0, Symbol.DOWNRIGHTXOFFSET);
		img.combine(0, 1, Symbol.VERTICALXOFFSET);
		img.combine(0, 2, Symbol.VERTICALXOFFSET);
		img.combine(0, 3, Symbol.UPRIGHTXOFFSET);
		
		img.combine(1, 1, Sprite.COIN);
		description=new Container("DESCRIPTION:\nBASE CURRENCY OF THE GAME",new TerminalPosition(19, 3),false);
		
		makebtn=new Button("MAKE", new TerminalPosition(3,18), false);
		
		makebtn.addClickListener(new Clickable() {
			
			@Override
			public void onClick() {
				beginning=System.currentTimeMillis();
				if (crafting) {
					progress=0;
				}
				crafting=!crafting;
				
				
			}
		});
		
		status=new Container("STATUS: AVAILABLE ", new TerminalPosition(12, 16), false);
		
		timer=new Container("TIME:    "+(int)timerequirement+"s", new TerminalPosition(51, 16), false);
		
		pb= new ProgressBar(51,0, new TerminalPosition(12, 18), false);
		add(itemlist,hline,ingredients,img,description,makebtn,pb,status,timer);
	}


	@Override
	public void update(double deltatime) {
		super.update(deltatime);
		pb.setprogress((float)progress/(float)timerequirement);
		if (crafting) {
			if (progress<timerequirement) {
				progress+=deltatime;
				makebtn.setText("STOP");
                status.setText("STATUS: CRAFTING ");
                String s=(int)Math.ceil(timerequirement-progress)+"";
                s="LEFT:"+TextUtil.repeat(" ",6-s.length())+s+"s";
                timer.setText(s);
			}else {
				Pet.coins++;
				progress=0;
				crafting=false;
			}
		}else {
			makebtn.setText("START");
            status.setText("STATUS: AVAILABLE ");
            timer.setText("TIME:    "+(int)timerequirement+"s");
		}
		
	}
	@Override
	public void activate() {
		super.activate();
		EventHandler.addClickable(makebtn);
	}
}
