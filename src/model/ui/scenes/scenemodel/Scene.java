package model.ui.scenes.scenemodel;

import java.awt.Window;

import com.googlecode.lanterna.gui2.AbsoluteLayout;
import com.googlecode.lanterna.gui2.BasicWindow;
import com.googlecode.lanterna.gui2.Component;
import com.googlecode.lanterna.gui2.LayoutManager;
import com.googlecode.lanterna.gui2.Panel;

import app.Game;
import model.EventHandler;
import model.SceneManager;
import model.ui.components.Container;
import model.ui.components.TopBar;
import util.Config;

public abstract class Scene {
	public Panel contentpanel;
	public BasicWindow window;
	public int sceneindex;
	
	public Scene(int index) {
		this.sceneindex=index;
		this.contentpanel=new Panel(new AbsoluteLayout());
		this.window=new BasicWindow();
		window.setHints(Game.config.hints);
		window.setTheme(Config.darktheme);	
		window.setComponent(contentpanel);
		
	}
	public void activate() {
		SceneManager.gui.setActiveWindow(SceneManager.scenes.get(sceneindex).window);
	};
	public void add(Container... c) {
		for (int i = 0; i < c.length; i++) {
			contentpanel.addComponent(c[i].getcomponent());
		}
	}
	public void update(double deltatime) {} ;
}
