package model.ui.scenes.scenemodel;

import model.EventHandler;
import model.ui.components.TopBar;

public class DefaultScene extends Scene{

	public TopBar bar;
	
	public DefaultScene(int index,String title) {
		super(index);
		this.bar=new TopBar(title);
		add(bar,bar.left,bar.right);
	}
	
	@Override
	public void activate() {
		super.activate();
		EventHandler.addClickable(bar.left,bar.right);
	}

	@Override
	public void update(double deltatime) {
		bar.update();
	}

}
