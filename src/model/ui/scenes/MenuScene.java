package model.ui.scenes;

import com.googlecode.lanterna.TerminalPosition;
import com.googlecode.lanterna.TerminalSize;
import com.googlecode.lanterna.graphics.Theme;
import com.googlecode.lanterna.gui2.BasicWindow;
import com.googlecode.lanterna.gui2.Border;
import com.googlecode.lanterna.gui2.Borders;
import com.googlecode.lanterna.gui2.Label;

import app.Game;
import model.Clickable;
import model.EventHandler;
import model.SceneManager;
import model.ui.components.Button;
import model.ui.components.Container;
import model.ui.scenes.scenemodel.Scene;
import util.Config;
import util.Symbol;

public class MenuScene extends Scene {
	Container title = new Container("Petgame"+Symbol.EGG,new TerminalPosition(28, 0),false);
	Button startbtn= new Button("    Start!    ", new TerminalPosition(24, 1),true);
	Button quitbtn= new Button("Close The Game",new TerminalPosition(24, 5),true);

	
	public MenuScene() {
		super(0);
		startbtn.addClickListener(new Clickable() {
			@Override
			public void onClick() {
				SceneManager.changeScene(SceneManager.main.sceneindex);
			}
		});
		
		
		
		quitbtn.addClickListener(new Clickable() {
			@Override
			public void onClick() {
				System.exit(0);
				
			}
		});
		
		add(startbtn,quitbtn,title);
	}


	@Override
	public void activate() {
		SceneManager.gui.setActiveWindow(SceneManager.menu.window);
		EventHandler.addClickable(startbtn,quitbtn);
	
	}


	@Override
	public void update(double deltatime) {
		// TODO Auto-generated method stub
		
	}
}
