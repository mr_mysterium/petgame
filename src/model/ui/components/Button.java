package model.ui.components;

import com.googlecode.lanterna.TerminalPosition;
import com.googlecode.lanterna.TerminalSize;
import model.Clickable;
import model.EventHandler;
import util.Config;

public class Button extends Container{
	Clickable c;
	EventHandler handler;
	boolean hoverable=true;
	public Button(String text,TerminalPosition pos,boolean bordered) {
		super(text, pos, bordered);
	}
	public Button(String text,TerminalPosition pos,boolean bordered,boolean hoverable) {
		super(text, pos, bordered);
		this.hoverable=hoverable;
	}
	
	public void addClickListener(Clickable c) {
		this.c=c;
	}
	
	public void onClick() {
		if (c==null)return;
		c.onClick();
	}
		
	
	
public TerminalSize getLabelSize() {
		return l.getSize();
	}
	
	public void setPosition(TerminalPosition position) {
		if (bordered) {
			b.setPosition(position);
			
		}
		else {
			l.setPosition(position);
		}
	}
	
	public void highlight() {
		if (!hoverable) {
			return;
		}
		if (bordered) {
			b.setTheme(Config.highlighted);
		}
		else {
			l.setTheme(Config.highlighted);
		}
	}
	public void unlight() {
		if (bordered) {
			 b.setTheme(Config.darktheme);
		}
		
		else {
			l.setTheme(Config.darktheme);
		}
		
		
	}

	public boolean ishoverable() {
		return hoverable;
	}

	public void sethoverable(boolean hoverable) {
		this.hoverable = hoverable;
	}
	
	

}
