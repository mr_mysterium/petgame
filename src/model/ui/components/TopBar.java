package model.ui.components;

import java.awt.Point;
import java.text.SimpleDateFormat;

import com.googlecode.lanterna.TerminalPosition;

import model.Clickable;
import model.EventHandler;
import model.SceneManager;
import pet.Pet;
import util.Symbol;
import util.TextUtil;

public class TopBar extends Container{
	public Container container;
	public Button left = new Button(Symbol.LTRIANGLE, new TerminalPosition(0,0),false);
	public Button right = new Button(Symbol.RTRIANGLE, new TerminalPosition(9,0),false);
	public TopBar(String scene) {
		super("", new TerminalPosition(0,0), false);
		
		
		
		String containerstring = TextUtil.generateBorder(new Point(64,8),new String[] {});
		container= new Container(containerstring,new TerminalPosition(0, 0),false);
		container.combine(10, 0,Symbol.VERTICAL+Symbol.CLOCK+"00:00"+TextUtil.repeat(" ", 45)+"0"+Symbol.COIN);
		
		StringBuilder hString=new StringBuilder(TextUtil.repeat(Symbol.HORIZONTAL, 64));
		hString.setCharAt(10,Symbol.aschar(Symbol.UPHORIZONTAL));
		
		container.combine(0, 1,hString.toString());
		container.combine(0, 0, scene);
		setText(container.getText());
		
		
		
		
		
		right.addClickListener(new Clickable() {
			
			@Override
			public void onClick() {
				int index=SceneManager.currentscene;
				index+=1;
				if (index<SceneManager.scenes.size()) {
					SceneManager.changeScene(index);
				}else {
					SceneManager.changeScene(1);
				}
			}
		});
		
		left.addClickListener(new Clickable() {
			
			@Override
			public void onClick() {
				int index=SceneManager.currentscene;
				index-=1;
				if (index>0) {
					SceneManager.changeScene(index);
				}else {
					SceneManager.changeScene(SceneManager.scenes.size()-1);
				}
				
			}
		});
	}
	
	public void setTitle(String scene) {
		container.combine(0, 0, scene);
		setText(container.getText());
	}
	
	public void update(){
		SimpleDateFormat format=new SimpleDateFormat("hh:mm");
		container.combine(12, 0, format.format(EventHandler.date));
		String s=Pet.coins+"";
		s=TextUtil.repeat(" ",12-s.length())+s;
		container.combine(51, 0, s);
		setText(container.getText());
	}
}
