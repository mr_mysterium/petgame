package model.ui.components;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import com.googlecode.lanterna.TerminalPosition;
import com.googlecode.lanterna.TerminalSize;
import com.googlecode.lanterna.TextColor;
import com.googlecode.lanterna.graphics.SimpleTheme;
import com.googlecode.lanterna.gui2.Border;
import com.googlecode.lanterna.gui2.Borders;
import com.googlecode.lanterna.gui2.Component;
import com.googlecode.lanterna.gui2.Label;

import util.MathUtil;

public class Container {
	Border b;
	Label l;
	boolean bordered;
	double brightness=1;
	
	public Container(String text,TerminalPosition pos,boolean bordered) {
		this.bordered=bordered;
		l = new Label(text);
		l.setSize(calculateSize());
		l.setPosition(pos);
		if (bordered) {
			b=l.withBorder(Borders.singleLine());
			b.setSize(calculateSize());
			l.setPosition(new TerminalPosition(0,0));
			b.setPosition(pos);
		}
	}
	
	public void setText(String text) {
		l.setText(text);
		l.setSize(calculateSize());
		if (bordered) {
			b.setSize(calculateSize());
		}
	}
	
	public String getText() {
			return l.getText();
	}
	
	public TerminalPosition getPosition() {
		if (bordered) {
			return b.getPosition();
		}
		
		return l.getPosition();
	}
	public TerminalPosition getLabelPosition() {
		return l.getGlobalPosition();
	}
	
	private TerminalSize calculateSize() {
		String[]lines=l.getText().split("\n");
		ArrayList<Integer>lengths=new ArrayList<>();
		int x,y;
		for (int i = 0; i < lines.length; i++) {
			lengths.add(lines[i].length());
		}
		x= Collections.max(lengths);
		y=lines.length;
		if (bordered) {
			x+=2;
			y+=2;
		}
		return new TerminalSize(x, y);
	}
	
	public TerminalSize getSize() {
		
		if (bordered) {
			return b.getSize();
		}else {
			return l.getSize();
		}
	}
	
	public void combine(int x,int y ,String s) {
		char[][]characters=new char[getSize().getRows()][getSize().getColumns()];
		String[]lines= l.getText().split("\n");
		String[]slines= s.split("\n");
		StringBuilder text= new StringBuilder();
		for (int i = 0; i < characters.length; i++) {
			for (int j = 0; j < characters[i].length; j++) {
				char c= ' ';
				if (i<lines.length&&j<lines[i].length()) {
					 c= lines[i].charAt(j);
				}
				characters[i][j]=c;
			}
		}
		for (int i = 0; i < characters.length; i++) {
			for (int j = 0; j < characters[i].length; j++) {
				if (i<slines.length&&j<slines[i].length()) {
					char c= slines[i].charAt(j);
					characters[i+y][j+x]=c;
				}
	
			}
		}
		
		for (int i = 0; i < characters.length; i++) {
			for (int j = 0; j < characters[i].length; j++) {
				text.append(characters[i][j]);
			}
				text.append("\n");
		}
		setText(text.toString());
	}
	
	public Component getcomponent() {
		if (bordered) {
			 return b;
		}
		
		else {
			return l;
		}
	}
	public Label getLabel() {
		return l;
	}
	
	public void setbrightness(double bright) {
		this.brightness=MathUtil.clamp(bright,0,1);
		int color=(int)Math.round(brightness*255);
		l.setTheme(new SimpleTheme(new TextColor.RGB(color,color,color), new TextColor.RGB(0, 0, 0)));
	}
	public double getbrightness() {
		return brightness;
	}
}
