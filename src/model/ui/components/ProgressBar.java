package model.ui.components;

import com.googlecode.lanterna.TerminalPosition;

import util.Symbol;
import util.TextUtil;

public class ProgressBar extends Container{

	public ProgressBar(int length,double val, TerminalPosition pos, boolean bordered) {
		super(Symbol.PROGRESSLEFT+TextUtil.repeat(Symbol.PROGRESSMIDDLE, length-2)+Symbol.PROGRESSRIGHT, pos, bordered);
		
	}
	public void setprogress(float progress){
		double p = Math.min(1.0, progress);
		int total=  (int) (Math.round(1000*p*getText().length())/1000);
		String s=(Symbol.PROGRESSLEFT+TextUtil.repeat(Symbol.PROGRESSMIDDLE, getText().length()-2)+Symbol.PROGRESSRIGHT);
		combine(0, 0, s);
		combine(0, 0, TextUtil.repeat(Symbol.BLOCKFULL, total));
		
	}

}
