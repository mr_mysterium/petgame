package model;

import java.awt.Dimension;
import java.awt.MouseInfo;
import java.awt.Point;
import java.awt.RenderingHints.Key;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Timer;
import java.util.concurrent.atomic.AtomicBoolean;

import com.googlecode.lanterna.TerminalPosition;
import com.googlecode.lanterna.TerminalSize;
import com.googlecode.lanterna.gui2.Window;
import com.googlecode.lanterna.gui2.WindowListener;
import com.googlecode.lanterna.input.KeyStroke;
import com.googlecode.lanterna.input.KeyType;

import app.Game;
import model.ui.components.Button;
import model.ui.scenes.MenuScene;
import util.MyTerminal;

public class EventHandler{
	Game game;
	public static boolean mousedown=false;
	public static ArrayList<Button>clickables= new ArrayList<>();
	public static Date date;
	public static int mousebutton;
	public static double mousedelta;
	public static Point mousepos=new Point(0,0);
	public static Thread input= new Thread(new Runnable() {
		@Override
		public void run() {
			
				
			
			while(true) {
				try {
				for (Button button : clickables)button.unlight();
			    for (Button button : intersecting())button.highlight();
					
					date=new Date();
					SceneManager.gui.updateScreen();
					SceneManager.updateScenes();
					Point mp=MouseInfo.getPointerInfo().getLocation();
					mousedelta=Math.sqrt((mp.distance(mousepos)));
					mousepos=mp;
					Thread.sleep(15);
					
					
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	});
	
	public EventHandler(Game game){
		this.game=game;
		
	}
	
	public static void clickEvent(Point p,Dimension d){
			if (intersecting().size()>0) {
				for (Button b : intersecting()) {
					b.onClick();
				}
				
					
			}
	}
	public static void keyEvent(KeyEvent e) {
		switch (e.getKeyCode()) {
		case KeyEvent.VK_ESCAPE:
			SceneManager.changeScene(SceneManager.menu.sceneindex);
			break;

		default:
			break;
		}
	}
	public static void addClickable(Button... c) {
		for (int i = 0; i < c.length; i++) {
			clickables.add(c[i]);
		}
	}
	public static void removeClickable(Button... c) {
		for (int i = 0; i < c.length; i++) {
			clickables.remove(c[i]);
		}
	}
	public static ArrayList<Button> intersecting() {
		
		ArrayList<Button>components=new ArrayList<>();
		try {
			
		
		int xtermsize=Game.term.getTerminalSize().getColumns();
		int ytermsize=Game.term.getTerminalSize().getRows();
		float xratio= mousepos.x/(float)MyTerminal.screensize.width;
		float yratio= mousepos.y/(float)MyTerminal.screensize.height;
		
		Point target= new Point((int)Math.round(xratio*xtermsize),(int) Math.round(yratio*ytermsize));
		
		for (int i = 0; i < clickables.size(); i++) {
			
			Button b= clickables.get(i);
			if (b==null) {
				continue;
			}
			
		if (target.x>=b.getLabelPosition().getColumn()&&target.x<=(b.getLabelPosition().getColumn()+b.getLabelSize().getColumns())&&
				target.y>=b.getLabelPosition().getRow()&&target.y<=(b.getLabelPosition().getRow()+b.getLabelSize().getRows())) {
				
				components.add(b);
			}
		}
		} catch (Exception e) {
			// TODO: handle exception
		}
		return components;
	}

}
