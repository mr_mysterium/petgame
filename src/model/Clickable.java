package model;

public interface Clickable {
	void onClick();
}
