package util;

	import com.googlecode.lanterna.terminal.swing.AWTTerminal;
import com.googlecode.lanterna.terminal.swing.AWTTerminalFontConfiguration;
import com.googlecode.lanterna.terminal.swing.AWTTerminalFrame;
import com.googlecode.lanterna.terminal.swing.SwingTerminal;
import com.googlecode.lanterna.terminal.swing.SwingTerminalFontConfiguration;
import com.googlecode.lanterna.terminal.swing.SwingTerminalFrame;
import com.googlecode.lanterna.terminal.swing.TerminalEmulatorAutoCloseTrigger;
import com.googlecode.lanterna.terminal.swing.TerminalEmulatorColorConfiguration;
import com.googlecode.lanterna.terminal.swing.TerminalEmulatorDeviceConfiguration;

import app.Game;
import model.EventHandler;

import com.googlecode.lanterna.SGR;
	import com.googlecode.lanterna.TerminalPosition;
	import com.googlecode.lanterna.graphics.TextGraphics;
	import com.googlecode.lanterna.input.KeyStroke;
	import com.googlecode.lanterna.input.KeyType;
	import com.googlecode.lanterna.terminal.IOSafeTerminal;
	import com.googlecode.lanterna.TerminalSize;
	import com.googlecode.lanterna.TextColor;
	import com.googlecode.lanterna.terminal.TerminalResizeListener;

	import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.Arrays;
	import java.util.EnumSet;
	import java.util.Set;
	import java.util.concurrent.TimeUnit;

import javax.swing.JFrame;

	
	
	@SuppressWarnings("serial")
	public class MyTerminal extends JFrame implements IOSafeTerminal,KeyListener{
		public static Dimension screensize;
	    private final SwingTerminal swingTerminal;
	    private final EnumSet<TerminalEmulatorAutoCloseTrigger> autoCloseTriggers;
	    private boolean disposed;

	    /**
	     * Creates a new SwingTerminalFrame with an optional list of auto-close triggers
	     * @param autoCloseTriggers What to trigger automatic disposal of the JFrame
	     */
	    @SuppressWarnings({"SameParameterValue", "WeakerAccess"})
	    public MyTerminal(TerminalEmulatorAutoCloseTrigger... autoCloseTriggers) {
	        this("MyTerminalFrame", autoCloseTriggers);
	    }

	    /**
	     * Creates a new SwingTerminalFrame with a specific title and an optional list of auto-close triggers
	     * @param title Title to use for the window
	     * @param autoCloseTriggers What to trigger automatic disposal of the JFrame
	     */
	    @SuppressWarnings("WeakerAccess")
	    public MyTerminal(String title, TerminalEmulatorAutoCloseTrigger... autoCloseTriggers) throws HeadlessException {
	        this(title, new SwingTerminal(), autoCloseTriggers);
	    }

	    /**
	     * Creates a new SwingTerminalFrame using a specified title and a series of swing terminal configuration objects
	     * @param title What title to use for the window
	     * @param deviceConfiguration Device configuration for the embedded SwingTerminal
	     * @param fontConfiguration Font configuration for the embedded SwingTerminal
	     * @param colorConfiguration Color configuration for the embedded SwingTerminal
	     * @param autoCloseTriggers What to trigger automatic disposal of the JFrame
	     */
	    public MyTerminal(String title,
	            TerminalEmulatorDeviceConfiguration deviceConfiguration,
	            SwingTerminalFontConfiguration fontConfiguration,
	            TerminalEmulatorColorConfiguration colorConfiguration,
	            TerminalEmulatorAutoCloseTrigger... autoCloseTriggers) {
	        this(title, null, deviceConfiguration, fontConfiguration, colorConfiguration, autoCloseTriggers);
	     
	    }

	    /**
	     * Creates a new SwingTerminalFrame using a specified title and a series of swing terminal configuration objects
	     * @param title What title to use for the window
	     * @param terminalSize Initial size of the terminal, in rows and columns. If null, it will default to 80x25.
	     * @param deviceConfiguration Device configuration for the embedded SwingTerminal
	     * @param fontConfiguration Font configuration for the embedded SwingTerminal
	     * @param colorConfiguration Color configuration for the embedded SwingTerminal
	     * @param autoCloseTriggers What to trigger automatic disposal of the JFrame
	     */
	    public MyTerminal(String title,
	                              TerminalSize terminalSize,
	                              TerminalEmulatorDeviceConfiguration deviceConfiguration,
	                              SwingTerminalFontConfiguration fontConfiguration,
	                              TerminalEmulatorColorConfiguration colorConfiguration,
	                              TerminalEmulatorAutoCloseTrigger... autoCloseTriggers) {
	        this(title,
	                new SwingTerminal(terminalSize, deviceConfiguration, fontConfiguration, colorConfiguration),
	                autoCloseTriggers);
	    }
	    
	    private MyTerminal(String title, SwingTerminal swingTerminal, TerminalEmulatorAutoCloseTrigger... autoCloseTriggers) {
	        super(title != null ? title : "Petgame");
	        this.swingTerminal = swingTerminal;
	        this.autoCloseTriggers = EnumSet.copyOf(Arrays.asList(autoCloseTriggers));
	        this.disposed = false;
	        swingTerminal.addKeyListener(this);
	        setExtendedState(JFrame.MAXIMIZED_BOTH);
	        setResizable(false);
	        setUndecorated(true);
	       
	        
	        Toolkit toolkit = Toolkit.getDefaultToolkit();
	        Image image = toolkit.getImage(getClass().getClassLoader().getResource("pointer.png"));
	        Cursor c = toolkit.createCustomCursor(image , new Point(0,0), "img");
	        setCursor (c);
	        
	        
	        getContentPane().setLayout(new BorderLayout());
	        getContentPane().add(swingTerminal, BorderLayout.CENTER);
	        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	        setBackground(Color.BLACK); //This will reduce white flicker when resizing the window
	        Thread update= new Thread(new Runnable() {
				
				@Override
				public void run() {
					while (true) {
					screensize= swingTerminal.getSize();
					try {
						Thread.sleep(15);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					}
				}
			});
	        update.start();
	        swingTerminal.addMouseListener(new MouseListener() {
				
				@Override
				public void mouseReleased(MouseEvent e) {
					EventHandler.mousedown=false;
				}
				
				@Override
				public void mousePressed(MouseEvent e) {
					EventHandler.mousebutton=e.getButton();
					EventHandler.clickEvent(e.getPoint(),swingTerminal.getSize());
					EventHandler.mousedown=true;
				}
				
				@Override
				public void mouseExited(MouseEvent e) {
					EventHandler.mousebutton=-1;
					EventHandler.mousedown=false;
					
				}
				
				@Override
				public void mouseEntered(MouseEvent e) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void mouseClicked(MouseEvent e) {
					// TODO Auto-generated method stub
					
				}
			
				
			});
	        
 addWindowListener(new WindowListener() {
				
				@Override
				public void windowOpened(WindowEvent e) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void windowIconified(WindowEvent e) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void windowDeiconified(WindowEvent e) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void windowDeactivated(WindowEvent e) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void windowClosing(WindowEvent e) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void windowClosed(WindowEvent e) {
					System.exit(0);
					
				}
				
				@Override
				public void windowActivated(WindowEvent e) {
					// TODO Auto-generated method stub
					
				}
			});
	    }

	    /**
	     * Returns the current font configuration. Note that it is immutable and cannot be changed.
	     * @return This {@link SwingTerminalFrame}'s current font configuration
	     */
	    public SwingTerminalFontConfiguration getFontConfiguration() {
	        return swingTerminal.getFontConfiguration();
	    }

	    /**
	     * Returns this terminal emulator's color configuration. Note that it is immutable and cannot be changed.
	     * @return This {@link SwingTerminalFrame}'s color configuration
	     */
	    public TerminalEmulatorColorConfiguration getColorConfiguration() {
	        return swingTerminal.getColorConfiguration();
	    }

	    /**
	     * Returns this terminal emulator's device configuration. Note that it is immutable and cannot be changed.
	     * @return This {@link SwingTerminalFrame}'s device configuration
	     */
	    public TerminalEmulatorDeviceConfiguration getDeviceConfiguration() {
	        return swingTerminal.getDeviceConfiguration();
	    }

	    /**
	     * Returns the auto-close triggers used by the SwingTerminalFrame
	     * @return Current auto-close trigger
	     */
	    public Set<TerminalEmulatorAutoCloseTrigger> getAutoCloseTrigger() {
	        return EnumSet.copyOf(autoCloseTriggers);
	    }

	    /**
	     * Sets the auto-close trigger to use on this terminal. This will reset any previous triggers. If called with
	     * {@code null}, all triggers are cleared.
	     * @param autoCloseTrigger Auto-close trigger to use on this terminal, or {@code null} to clear all existing triggers
	     * @return Itself
	     */
	    public MyTerminal setAutoCloseTrigger(TerminalEmulatorAutoCloseTrigger autoCloseTrigger) {
	        this.autoCloseTriggers.clear();
	        if(autoCloseTrigger != null) {
	            this.autoCloseTriggers.add(autoCloseTrigger);
	        }
	        return this;
	    }

	    /**
	     * Adds an auto-close trigger to use on this terminal.
	     * @param autoCloseTrigger Auto-close trigger to add to this terminal
	     * @return Itself
	     */
	    public MyTerminal addAutoCloseTrigger(TerminalEmulatorAutoCloseTrigger autoCloseTrigger) {
	        if(autoCloseTrigger != null) {
	            this.autoCloseTriggers.add(autoCloseTrigger);
	        }
	        return this;
	    }

	    @Override
	    public void dispose() {
	        super.dispose();
	        disposed = true;
	    }
	    
	    @Override
	    public void pack() {
	        super.pack();
	        disposed = false;
	    }

	    @Override
	    public void setVisible(boolean visible) {
	        if (visible) {
	            pack();

	            //Put input focus on the terminal component by default
	            swingTerminal.requestFocusInWindow();
	        }
	        super.setVisible(visible);
	    }

	    @Override
	    public void close() {
	        dispose();
	    }

	    /**
	     * Takes a KeyStroke and puts it on the input queue of the terminal emulator. This way you can insert synthetic
	     * input events to be processed as if they came from the user typing on the keyboard.
	     * @param keyStroke Key stroke input event to put on the queue
	     */
	    public void addInput(KeyStroke keyStroke) {
	        swingTerminal.addInput(keyStroke);
	    }

	    ///////////
	    // Delegate all Terminal interface implementations to SwingTerminal
	    ///////////
	    @Override
	    public KeyStroke pollInput() {
	        if(disposed) {
	            return new KeyStroke(KeyType.EOF);
	        }
	        KeyStroke keyStroke = swingTerminal.pollInput();
	        if(autoCloseTriggers.contains(TerminalEmulatorAutoCloseTrigger.CloseOnEscape) &&
	                keyStroke != null && 
	                keyStroke.getKeyType() == KeyType.Escape) {
	            dispose();
	        }
	        return keyStroke;
	    }

	    @Override
	    public KeyStroke readInput() {
	        return swingTerminal.readInput();
	    }

	    @Override
	    public void enterPrivateMode() {
	        swingTerminal.enterPrivateMode();
	    }

	    @Override
	    public void exitPrivateMode() {
	        swingTerminal.exitPrivateMode();
	        if(autoCloseTriggers.contains(TerminalEmulatorAutoCloseTrigger.CloseOnExitPrivateMode)) {
	            dispose();
	        }
	    }

	    @Override
	    public void clearScreen() {
	        swingTerminal.clearScreen();
	    }

	    @Override
	    public void setCursorPosition(int x, int y) {
	        swingTerminal.setCursorPosition(x, y);
	    }

	    @Override
	    public void setCursorPosition(TerminalPosition position) {
	        swingTerminal.setCursorPosition(position);
	    }

	    @Override
	    public TerminalPosition getCursorPosition() {
	        return swingTerminal.getCursorPosition();
	    }

	    @Override
	    public void setCursorVisible(boolean visible) {
	        swingTerminal.setCursorVisible(visible);
	    }

	    @Override
	    public void putCharacter(char c) {
	        swingTerminal.putCharacter(c);
	    }

	    @Override
	    public void putString(String string) {
	        swingTerminal.putString(string);
	    }

	    @Override
	    public TextGraphics newTextGraphics() {
	        return swingTerminal.newTextGraphics();
	    }

	    @Override
	    public void enableSGR(SGR sgr) {
	        swingTerminal.enableSGR(sgr);
	    }

	    @Override
	    public void disableSGR(SGR sgr) {
	        swingTerminal.disableSGR(sgr);
	    }

	    @Override
	    public void resetColorAndSGR() {
	        swingTerminal.resetColorAndSGR();
	    }

	    @Override
	    public void setForegroundColor(TextColor color) {
	        swingTerminal.setForegroundColor(color);
	    }

	    @Override
	    public void setBackgroundColor(TextColor color) {
	        swingTerminal.setBackgroundColor(color);
	    }

	    @Override
	    public TerminalSize getTerminalSize() {
	        return swingTerminal.getTerminalSize();
	    }

	    @Override
	    public byte[] enquireTerminal(int timeout, TimeUnit timeoutUnit) {
	        return swingTerminal.enquireTerminal(timeout, timeoutUnit);
	    }

	    @Override
	    public void bell() {
	        swingTerminal.bell();
	    }

	    @Override
	    public void flush() {
	        swingTerminal.flush();
	    }

	    @Override
	    public void addResizeListener(TerminalResizeListener listener) {
	        swingTerminal.addResizeListener(listener);
	    }

	    @Override
	    public void removeResizeListener(TerminalResizeListener listener) {
	        swingTerminal.removeResizeListener(listener);
	    }

		@Override
		public void keyPressed(KeyEvent e) {
			EventHandler.keyEvent(e);
			
		}

		@Override
		public void keyReleased(KeyEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void keyTyped(KeyEvent e) {
			// TODO Auto-generated method stub
			
		}
	  
}
