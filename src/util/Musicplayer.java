package util;

import java.io.File;
import java.io.InputStream;
import java.net.URI;

import javafx.application.Application;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.stage.Stage;

public class Musicplayer extends Application{
	public MediaPlayer player;
	public Media bgmusic;


	@Override
	public void start(Stage primaryStage) throws Exception {
		String musicstring="assets/gsf.mp3";
		URI is = getClass().getClassLoader().getResource("gsf.mp3").toURI();
		bgmusic = new Media(is.toString());
		player = new MediaPlayer(bgmusic);
		player.setCycleCount(MediaPlayer.INDEFINITE);
		player.play();
		player.setAutoPlay(true);
		player.setVolume(0.);
		
	}

}
