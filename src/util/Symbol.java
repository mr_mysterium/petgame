package util;

public class Symbol {
	public final static String
		HORIZONTAL="\u2500",
		VERTICAL="\u2502",
		UPHORIZONTAL="\u2534",
		VERTICALLEFT="\u2524",
		VERTICALRIGHT= "\u251C",
		VERTICALHORIZONTAL="\u253c",
		UPRIGHT="\u2514",
		UPLEFT="\u2518",
		DOWNRIGHT="\u250C",
		DOWNLEFT="\u2510",
		DOWNHORIZONTAL="\u252c",
		CLOCK = "\u231b",
		COIN = "\u26c2",
	    EGG= "\ue1b0",
		LTRIANGLE="\ue2cd",
	    RTRIANGLE="\ue2ce",
		PROGRESSLEFT="\ue920",
		PROGRESSMIDDLE="\ue921",
		PROGRESSRIGHT="\ue922",
		BLOCKFULL="\u2584",
		UPARROW="\u2191",
		SMILEY="\ue923",
		HORIZONTALYOFFSET="\u2501",
		UPRIGHTYOFFSET="\u2517",
		UPLEFTYOFFSET="\u251B",
		VERTICALXOFFSET="\u2503",
		DOWNRIGHTXOFFSET="\u250d",
		UPRIGHTXOFFSET="\u2515";
		
	public static char aschar(String s) {
		return s.charAt(0);
	}
		
	
}
