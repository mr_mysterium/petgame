package util;

import java.awt.Point;
import java.util.Arrays;

import model.ui.components.Container;

public class TextUtil {
	public static String repeat(String s,int n) {
		StringBuilder torepeat=new StringBuilder();
		for (int i = 0; i < n; i++) {
			torepeat.append(s);
		}
		return torepeat.toString();
	}
	public static String timeString(long millis) {
		long timepassed=0;
		if (millis<60000) {
			timepassed=millis/1000;
			return timepassed+"s";
		}else if (millis < 3600000) {
			timepassed = millis /60000;
			return timepassed+"m";
		}else if (millis < 86400000) {
			timepassed = millis /3600000;
			return timepassed+"h";
		}else {
			timepassed= millis/86400000;
			return timepassed+"d";
		}
	}
	public static String generateBorder(Point size,String[]lines) {
		int[][]blueprint=new int[size.y][size.x];
		
		
		for (int i = 0; i < lines.length; i++) {
			String[]data=lines[i].split(",");
			int x1= Integer.parseInt(data[0]);
			int y1= Integer.parseInt(data[1]);
			int x2= Integer.parseInt(data[2]);
			int y2= Integer.parseInt(data[3]);
			for (int j = y1; j <= y2 ; j++) {
				for (int k = x1; k <= x2; k++) {
					blueprint[j][k]=1;
				}
			}
		}
		return generateBorder(blueprint);
	}
	public static String generateBorder(int[][]blueprint) {
		StringBuilder s=new StringBuilder();
		String[][]result=new String[blueprint.length][blueprint[0].length];
		for (int i = 0; i < blueprint.length; i++) {
			for (int j = 0; j < blueprint[i].length; j++) {
				int top = 0;
				int bottom=0;
				int left=0; 
				int right=0;
				if (i>0)top=blueprint[i-1][j];
				if (i<blueprint.length-1)bottom=blueprint[i+1][j];
				if (j>0)left=blueprint[i][j-1];
				if (j<blueprint[i].length-1)right=blueprint[i][j+1];
				String code=""+top+right+bottom+left;
			
				if (blueprint[i][j]==0) {
					result[i][j]=" ";

				}else {
					
				
				switch (code) {
				case "0100":
					result[i][j]=Symbol.HORIZONTAL;
				case "0101":
					result[i][j]=Symbol.HORIZONTAL;
					break;
				case "1010":
					result[i][j]=Symbol.VERTICAL;
					break;
				case "1100":
					result[i][j]=Symbol.UPRIGHT;
					break;
				case "1001":
					result[i][j]=Symbol.UPLEFT;
					break;
				case "0110":
					result[i][j]=Symbol.DOWNRIGHT;
					break;
				case "0011":
					result[i][j]=Symbol.DOWNLEFT;
					break;
				case "1110":
					result[i][j]=Symbol.VERTICALRIGHT;
					break;
				case "1011":
					result[i][j]=Symbol.VERTICALLEFT;
					break;
				case "1101":
					result[i][j]=Symbol.UPHORIZONTAL;
					break;
				case "0111":
					result[i][j]=Symbol.DOWNHORIZONTAL;
					break;
				case "1111":
					result[i][j]=Symbol.VERTICALHORIZONTAL;
					break;
				default:
					result[i][j]=" ";
					break;
					}
				}
			}
		}
		for (int k = 0; k < result.length; k++) {
			for (int l = 0; l < result[0].length; l++) {
				s.append(result[k][l]);
			}
			s.append("\n");
		}
		return s.toString();
	}
}
