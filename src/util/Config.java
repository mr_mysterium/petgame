package util;

import java.awt.Font;
import java.io.File;
import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;

import com.googlecode.lanterna.TerminalSize;
import com.googlecode.lanterna.TextColor;
import com.googlecode.lanterna.graphics.SimpleTheme;
import com.googlecode.lanterna.gui2.Window;
import com.googlecode.lanterna.gui2.Window.Hint;
import com.googlecode.lanterna.terminal.swing.AWTTerminalFontConfiguration;
import com.googlecode.lanterna.terminal.swing.SwingTerminalFontConfiguration;

import app.Game;

import com.googlecode.lanterna.terminal.swing.AWTTerminalFontConfiguration.BoldMode;

public class Config {
	public Game game;
	public  Font[] fonts;
	public  AWTTerminalFontConfiguration fontconfig;
	public  static SimpleTheme darktheme = new SimpleTheme(new TextColor.RGB(255, 255, 255), new TextColor.RGB(0, 0, 0));
	public  static SimpleTheme highlighted = new SimpleTheme(new TextColor.RGB(255, 0, 0), new TextColor.RGB(0, 0, 0));
	public  ArrayList<Hint> hints= new ArrayList<>(Arrays.asList(Window.Hint.NO_DECORATIONS,Window.Hint.FULL_SCREEN));
	public  Config(Game game) {
		try {
			this.game=game;
			Font fs = Font.createFont(Font.TRUETYPE_FONT,getClass().getClassLoader().getResourceAsStream("fs.ttf"));
			Font kong = Font.createFont(Font.TRUETYPE_FONT, getClass().getClassLoader().getResourceAsStream("kong.ttf"));
			fs=fs.deriveFont(60f);
			kong=kong.deriveFont(60f);
			fonts=new Font[]{fs,kong};
			fontconfig= new SwingTerminalFontConfiguration(false, BoldMode.NOTHING,fonts);
			game.factory.setTerminalEmulatorFontConfiguration(fontconfig);
			game.factory.setInitialTerminalSize(new TerminalSize(200, 100));
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

}
