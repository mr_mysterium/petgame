package util;

public class Sprite {

	public static String 
	DUCK=buildImage("e000",2,2),
	BIGDUCK=buildImage("e900",8,4),
	COIN=buildImage("e924", 4, 2);

	public static String buildImage(String start,int x,int y) {
		int hex=Integer.parseInt(start,16);
	    StringBuilder s=new StringBuilder();
	    for (int i = 0; i <y; i++) {
			for (int j = 0; j < x; j++) {
				s.append((char)hex);
				hex++;
			}
			s.append("\n");
		}
	  return s.toString();
		
	}
	
}
